from flask import Flask, render_template, redirect, url_for, request, abort
from flask.helpers import flash

from forms import AddCarroForm

app = Flask(__name__)
app.secret_key = 'VyUc4ir0dFulpxKzPpPSbw'

fake_db = [
    { "id": 1, "marca": "Ford", "modelo": "Focus", "ano": 2017 },
    { "id": 2, "marca": "Ford", "modelo": "Focus", "ano": 2013 }
]

@app.route("/", methods=['GET'])
def index():
    form = AddCarroForm()
    return render_template('index.html', carros=fake_db, form=form)

@app.route("/", methods=['POST'])
def create():
    form = AddCarroForm(request.form)

    if not form.validate():
        flash(message="Verifique os campos!", category='is-danger')
        return render_template('index.html', carros=fake_db, form=form)
        
    carro = { "id": len(fake_db) + 1, "marca": form.marca.data, "modelo": form.modelo.data, "ano": form.ano.data }
    fake_db.append(carro)
    flash(message="Carro criado com sucesso!", category='is-success')
    return redirect(url_for('index')) 

@app.route("/carros/delete/<int:id>", methods=['POST'])
def delete(id):
    carro = [ x for x in fake_db if x['id'] == id ]

    if not carro:
        abort(404)
        
    fake_db.remove(carro[0])    

    return redirect(url_for('index'))

@app.route("/carros/edit/<int:id>", methods=['GET','POST'])
def edit(id):
    form = AddCarroForm(request.form)    

    carro = [ x for x in fake_db if x['id'] == id ]

    if not carro:
        abort(404)

    if request.method == 'POST': 
        if not form.validate():
            flash(message="Verifique os campos!", category='is-danger')
            return render_template('edit.html', form=form, id = id)
        else:            
            carro_edit = { "id": id, "marca": form.marca.data, "modelo": form.modelo.data, "ano": form.ano.data }
            fake_db.remove(carro[0])    
            fake_db.append(carro_edit)
            flash(message="Carro editado com sucesso!", category='is-success')
            return redirect(url_for('index'))

    form.marca.data = carro[0]['marca']
    form.modelo.data = carro[0]['modelo']
    form.ano.data = carro[0]['ano']            

    return render_template('edit.html', form=form, id = id)   

@app.errorhandler(404)
def page_not_found(error):
    return render_template('page_not_found.html'), 404
