from wtforms import Form, StringField, validators, IntegerField
from datetime import datetime

class AddCarroForm(Form):
    msg = "* Campo requerido"
    marca = StringField('Marca', [validators.Length(min=2, max=25, message=msg)])
    modelo = StringField('Modelo', [validators.Length(min=2, max=25, message=msg)])
    ano = IntegerField('Ano',[validators.NumberRange(min=1900, max=datetime.now().year, message="* Ano inválido")])
    
